#!/usr/bin/env python
from collections import Counter
from math import log

def shannon_p(p):
    return p * log(p, 2)

def shannon(data):
    freq = frequency(data)
    summed = sum([shannon_p(p) for p in freq.values()])
    return -1 * summed / 8.0

def frequency(data):
    ctr = Counter(data)
    size = len(data)
    freq = {}
    for char, ct in ctr.items():
        freq[char] = float(ct) / size
    return freq

def shannon_file(path):
    with open(path) as f:
        data = f.read()
    return shannon(data)
