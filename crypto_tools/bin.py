#!/usr/bin/env python
import entropy

def crypto_entropy():
    import argparse
    parser = argparse.ArgumentParser()
    parser.add_argument('path')
    args = parser.parse_args()
    ent = entropy.shannon_file(args.path)
    print(ent)
    
def crypto_tools():
    import argparse
    parser = argparse.ArgumentParser()
    args = parser.parse_args()
